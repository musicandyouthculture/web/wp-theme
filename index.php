<?php

$context = Timber::context();
$context['posts'] = new Timber\PostQuery([
  'post_type' => 'post',
  'paged' => get_query_var('paged') ?: 1,
]);

$templates = ['index.twig'];
if (is_home()) {
  $context['post'] = new Timber\Post();
  array_unshift($templates, 'home.twig');
}

Timber::render($templates, $context);

