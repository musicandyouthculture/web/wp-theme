#!/bin/bash

# build theme
composer install
npm i && npm start

# build fotos
cd lib/Media/Images/src
composer install
cd assets/fotos
npm i && npm run prod
cd ../../../../../..

# zip
composer archive -f zip --file ifmayc

