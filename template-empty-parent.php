<?php
/**
 * Template Name: Dummy Parent
 */

$children = get_children([
    'post_parent' => get_the_ID(),
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_type' => 'page',
]);
$children = array_values($children);

wp_redirect(get_permalink($children[0]), 301);
