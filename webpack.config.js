const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = {
  entry: {
    main: './assets/css/main.scss',
  },

  output: {
    publicPath: `/${__dirname.split(path.sep).slice(-3).join(path.sep)}/dist/`,
  },

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
	  {
	    loader: 'css-loader',
	    options: {
	      url: false,
	    },
	  },
          'sass-loader',
        ],
      },
    ],
  },

  plugins: [
    new MiniCssExtractPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        { from: __dirname + '/assets/img', to: 'img' },
        { from: __dirname + '/assets/fonts', to: 'fonts' },
      ],
    }),
  ],

  optimization: {
    minimizer: [
      `...`,
      new CssMinimizerPlugin(),
    ],
  },
};
