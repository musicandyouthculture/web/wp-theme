<?php

namespace ifmayc\Gigadmin;

class Event
{
    /**
     * Get single event
     *
     * @param string $where
     *
     * @return mixed
     */
    public static function get($where = '1=1')
    {
        global $gigdb;

        $event = $gigdb->get_row("SELECT * FROM gigs WHERE $where");

        if ($event) {
            $event = self::formatEvent($event);
        }

        return $event;
    }

    /**
     * Get multiple events
     *
     * @param string $where
     *
     * @return mixed
     */
    public static function all($where = '1=1')
    {
        global $gigdb;

        $events = $gigdb->get_results("SELECT * FROM gigs WHERE $where");
        array_map([__CLASS__, 'formatEvent'], $events);

        return $events;
    }

    /**
     * Get next upcoming event
     *
     * @return mixed
     */
    public static function getNext()
    {
        return self::get("datum >= CURRENT_DATE() AND website = 1 AND sidebar = 1 ORDER BY datum ASC LIMIT 1");
    }

    /**
     * Get all upcoming events
     *
     * @return mixed
     */
    public static function getUpcoming()
    {
        return self::all('datum >= CURRENT_DATE() AND website = 1 ORDER BY datum ASC');
    }

    /**
     * Get all past events
     *
     * @return mixed
     */
    public static function getPast()
    {
	return self::all('datum < CURRENT_DATE() AND website = 1 ORDER BY datum DESC');
    }

    /**
     * @param $event
     *
     * @return mixed
     */
    public static function formatEvent($event)
    {
        $event->bands = self::getBands($event);
        $event->flyerPreview = preg_replace('/(.*)\_(.)(.*)/i', '$1_q$3', $event->flyer);
        $event->genres = array_filter(array_map('trim', explode(',', $event->hashtag_genre)));

        return $event;
    }

    /**
     * Transform bands to usable array
     *
     * @param $event
     *
     * @return array
     */
    public static function getBands($event)
    {
        $events = [];
        for ($i = 1; $i <= 8; $i++) {
            if (empty($event->{"band{$i}name"})) {
                continue;
            }

            $events[] = [
                'name' => $event->{"band{$i}name"},
                'gage' => $event->{"band{$i}gage"},
                'doordeal' => $event->{"band{$i}doordeal"},
                'uebernachtung' => $event->{"band{$i}uebernachtung"},
                'anzahl' => $event->{"band{$i}anzahl"},
                'essen' => $event->{"band{$i}essen"},
                'facebook' => $event->{"band{$i}facebook"},
                'hoerproben' => $event->{"band{$i}hoerproben"},
                'backline' => $event->{"band{$i}backline"},
                'anmerkungen' => $event->{"band{$i}anmerkungen"},
            ];
        }

        return $events;
    }
}
