<?php

namespace ifmayc\Gigadmin;

use Symfony\Component\Serializer\Encoder\XmlEncoder;

class RSS
{
    public function __construct()
    {
        add_action('template_redirect', function () {
            if (str_contains($_SERVER['REQUEST_URI'], 'events/feed.xml')) {
                $this->buildFeed();
                die();
            }
        });
    }

    public function buildFeed()
    {
        // Set CORS headers
        header("Access-Control-Allow-Origin: *"); // Erlaubt den Zugriff von allen Domains
        header("Access-Control-Allow-Methods: GET"); // Nur GET-Anfragen zulassen
        header("Access-Control-Allow-Headers: Content-Type"); // Content-Type-Header erlauben

        $feed = [
            '@xmlns:media' => 'http://search.yahoo.com/mrss/',
            'channel' => [
                'title' => 'musicandyouthculture - Events',
                'link' => 'https://musicandyouthculture.de/events',
                'description' => 'Die neuesten Events bei Music and Youth Culture',
                'language' => 'de-DE',
                'generator' => 'Custom RSS Feed',
                'lastBuildDate' => date(DATE_RSS),
            ],
        ];

        $events = Event::getUpcoming();
        if (count($events) > 0) {
            $feed['channel']['item'] = [];
        }

        foreach ($events as $event) {
            setlocale(LC_TIME, 'de_DE');
            $date = strftime('%a, %d.%m.%Y', strtotime($event->datum));

            $title = $event->veranstaltung;

            if (!empty($event->bands)) {
                $title .= ' mit ' . implode(
                    ', ',
                    array_map(function ($band) {
                        return $band['name'];
                    }, $event->bands)
                );
            }

            $content = \Timber\Timber::fetch('rss.twig', compact('event', 'date'));

            setlocale(LC_TIME, 'de_DE');

            // Datum und Zeit kombinieren und Zeitzone festlegen
            $eventDateTime = new \DateTime($event->datum . ' ' . $event->zeit, new \DateTimeZone('Europe/Berlin'));

            // Im deutschen Format mit Zeit ausgeben und die richtige Zeitzone anwenden
            $pubDate = strftime('%a, %d %b %Y %H:%M:%S %z', $eventDateTime->getTimestamp());

            $feed['channel']['item'][] = [
                'title' => $title,
                'link' => 'https://musicandyouthculture.de/events',
                'guid' => crc32($event->id),
                'pubDate' => $pubDate,
                'description' => $content,
                'media:thumbnail' => [
                    '@url' => $event->flyer ?: ifmaycThemeAssetsUrl('img/platzhalter.png'),
                ],
            ];
        }

        $encoder = new XmlEncoder();
        $rss = $encoder->encode($feed, XmlEncoder::FORMAT, [
            XmlEncoder::ROOT_NODE_NAME => 'rss',
            XmlEncoder::ENCODING => 'utf-8',
            'xml_format_output' => true,
            'remove_empty_tags' => true,
            'xml_version' => '1.0',
        ]);

        header('Content-Type: text/xml');
        http_response_code(200);
        echo $rss;
    }
}
