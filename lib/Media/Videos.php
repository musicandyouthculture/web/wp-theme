<?php

namespace ifmayc\Media;

use PostTypes\Metabox;
use PostTypes\MetaboxField;
use PostTypes\PostType;
use PostTypes\Taxonomy;
use Timber\PostQuery;
use Timber\Timber;

class Videos
{
    /**
     * Videos constructor.
     */
    public function __construct()
    {
        $videos = new PostType('video', [
            'supports' => ['title', 'thumbnail'],
        ]);
        $videos->icon('dashicons-video-alt');
        $videos->register();

        $videoMeta = new Metabox('Video Meta');
        $videoMeta->posttype('video');
        $videoMeta->field((new MetaboxField('video_links'))->type('text'));
        $videoMeta->field((new MetaboxField('bands_names'))->type('text'));
        $videoMeta->field((new MetaboxField('bands_social_links'))->type('text'));
        $videoMeta->field((new MetaboxField('use_bands_as_title', ['default_value' => 1, 'label' => 'Use Bands as title?']))->type('true_false'));
        $videoMeta->add();

        $genre = new Taxonomy('genre');
        $genre->posttype('video');
        $genre->register();

	// disable single
	add_action('template_redirect', function () {
	    if (is_singular('video')) {
		wp_redirect('/videos', 301);
		exit;
	    }
	});

        add_shortcode('ifmayc_media_videos', [$this, 'shortcodeIfmaycMediaVideos']);
    }


    /**
     * @return bool|string
     */
    public function shortcodeIfmaycMediaVideos()
    {
        $videos = new PostQuery([
            'post_type' => 'video',
            'paged' => get_query_var('paged') ?: 1,
        ]);

        return Timber::fetch('shortcodes/ifmayc-media/videos/index.twig', compact('videos'));
    }
}
