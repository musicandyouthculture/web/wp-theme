<?php

namespace ifmayc\Media\Images;

use JUZE\Medien\API;
use JUZE\Medien\Settings;
use JUZE\Medien\Utilities;

class Fotos
{
    /**
     * Fotos constructor.
     */
    public function __construct()
    {
        require_once __DIR__ . '/src/vendor/autoload.php';

				Settings::getInstance();

				add_action('wp_ajax_medien_fotos', [API::class, 'entryPoint']);
				add_action('wp_ajax_nopriv_medien_fotos', [API::class, 'entryPoint']);
        add_shortcode('ifmayc_media_images', [$this, 'shortcodeIfmaycMediaImages']);
    }

    /**
     * Load Images submodule
     *
     * @return string
     */
    public function shortcodeIfmaycMediaImages()
    {
        wp_enqueue_style('font-awesome', Utilities::assetsUrl('font-awesome.min.css'));
        wp_enqueue_style('vue-cool-lightbox', Utilities::assetsUrl('vue-cool-lightbox.min.css'));

        wp_enqueue_script('vue', Utilities::assetsUrl('vue.min.js'));
        wp_enqueue_script('vue-router', Utilities::assetsUrl('vue-router.min.js'), ['vue']);
        wp_enqueue_script('vuex', Utilities::assetsUrl('vuex.min.js'), ['vue']);

        wp_enqueue_style('medien-fotos', Utilities::assetsUrl('medien_fotos.css'));
        wp_enqueue_script('medien-fotos', Utilities::assetsUrl('medien_fotos.js'), ['jquery', 'vue', 'vue-router', 'vuex'], null, true);
				wp_add_inline_script('medien-fotos', 'var medien_fotos_settings =' . json_encode(Settings::getInstance()->get(), true), 'before');

        wp_add_inline_script('medien-fotos', 'var ajaxUrl ="' . admin_url('admin-ajax.php') . '"', 'before');

        return '<div id="fotos"></div>';
    }
}
