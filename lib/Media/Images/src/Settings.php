<?php
/**
 * Created by PhpStorm.
 * User: dweipert
 * Date: 05.01.2019
 * Time: 17:05
 */

namespace JUZE\Medien;

use Dotenv\Dotenv;
use PetrKnap\Php\Singleton\SingletonTrait;

class Settings
{
    use SingletonTrait;

    private static $settings;

    protected function __construct() {
				if (! defined('MEDIA__IMAGES__FLICKR_API_KEY')) {
						wp_die(
								'<h1>Media Images variables missing</h1>' .
										'<ul>' .
										'<li>MEDIA__IMAGES__FLICKR_API_KEY</li>' .
								'</ul>'
						);
				}

				! defined('MEDIA__IMAGES__FLICKR_API_CACHE_TIME') && define('MEDIA__IMAGES__FLICKR_API_CACHE_TIME', '-5 minutes');
				! defined('MEDIA__IMAGES__TITLE_MAX_LENGTH') && define('MEDIA__IMAGES__TITLE_MAX_LENGTH', 50);

        global $gigdb;
        $query = "
          (SELECT datum FROM gigadmin.gigs WHERE flickr > 0 ORDER BY datum ASC LIMIT 1)
          UNION ALL
          (SELECT datum FROM gigadmin.gigs WHERE flickr > 0 ORDER BY datum DESC LIMIT 1)
          ";
        $events = $gigdb->get_results($query);

        self::$settings = [
            'minYear' => intval(date('Y', strtotime($events[0]->datum))),
            'maxYear' => intval(date('Y', strtotime($events[1]->datum))),
            'flickrApiAvailable' => Flickr::isAvailable(),
        ];
    }

    public function get()
    {
        return self::$settings;
    }

    public function __get($name)
    {
        return self::$settings[$name];
    }
}

