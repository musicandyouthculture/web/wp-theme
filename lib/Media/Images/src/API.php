<?php
/**
* Created by PhpStorm.
* User: dweipert
* Date: 04.01.2019
* Time: 13:12
*/

namespace JUZE\Medien;

use JUZE\Medien\Flickr;
use JUZE\Medien\Settings;
use JUZE\Medien\Utilities;

class API
{
		public static function entryPoint()
		{
				call_user_func([__CLASS__, $_POST['method']]);
		}

		public static function getAlbums()
		{
				global $gigdb;

				#$query = 'SELECT * FROM fotos.lychee_albums WHERE public = 1 ORDER BY sysstamp DESC';
				$query = $gigdb->preapre('SELECT * FROM gigadmin.gigs WHERE flickr > 0 ORDER BY datum DESC');
				$results = $gigdb->get_results($query);	

				$albums = [];
				foreach ($results as $row) {
						$album = Utilities::prepareAlbum($row);

						$albums[] = $album;
				}

				Utilities::sendResponse($albums);
		}

		public static function getAlbumsByDate()
		{
				global $gigdb;

				$year = intval($_POST['year']);

				$albums = [];
				$query = $gigdb->prepare('SELECT * FROM gigadmin.gigs WHERE flickr > 0 AND year(datum) = %d ORDER BY datum DESC', $year);
				$results = $gigdb->get_results($query);

				foreach ($results as $row) {
						$album = Utilities::prepareAlbum($row);

						$albums[] = $album;
				}

				Utilities::sendResponse($albums);
		}

		public static function getAlbum()
		{
				global $gigdb;

				$id = intval($_POST['id']);

				$queryAlbum = $gigdb->prepare('SELECT * from gigadmin.gigs WHERE flickr = %s', $id);
				$albumRow = $gigdb->get_row($queryAlbum);

				$album = [];
				$album = [
						'id' => $id,
						'date' => date('d.m.Y', strtotime($albumRow->datum)),
						'title' => Utilities::getAlbumTitle($albumRow),
						'images' => [],
						'credits' => Utilities::parseCredits($albumRow->credits),
						'flickr' => [
								'url' => Flickr::getFlickrAlbumUrl($id),
						],
				];

				$images = Flickr::getAlbum($album['id']);
				$images = json_decode($images)->photoset->photo;

				foreach ($images as $image) {
						$album['images'][] = [
								'id' => $image->id,
								'title' => $image->title,
								'thumbUrl' => Flickr::getStaticPhotoUrl($image, 'Large Square'),
								'url' => Flickr::getStaticPhotoUrl($image, 'Large'),
						];
				}

				Utilities::sendResponse($album);
		}
}
