/**
 * Created by dweipert on 05.01.19.
 */

const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const isProduction = process.env.NODE_ENV === 'production';

// main config
const config = {
    mode: isProduction ? 'production' : 'development',

    entry: {},

    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js'
    },

    resolve: {
        extensions: ['.js', '.vue']
    },

    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        ['@babel/preset-env', {
                            targets: {
                                browsers: ['> 1%', 'last 2 versions']
                            }
                        }]
                    ]
                },
                exclude: path.resolve(__dirname, 'node_modules')
            },
            {
                test: /\.s[ac]ss$/,
                loader: ExtractTextPlugin.extract({
                    use: [
                        { loader: 'css-loader' },
                        { loader: 'postcss-loader',
                            options: {
                                plugins: () => {
                                    let plugins = [
                                        require('autoprefixer')(),
                                    ];
                                    if (isProduction) {
                                        plugins.push(require('cssnano')({ preset: 'default' }));
                                    }
                                    return plugins;
                                }
                            } },
                        { loader: 'sass-loader' }
                    ]
                })
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            }
        ]
    },

    plugins: [
        new VueLoaderPlugin(),
        new ExtractTextPlugin({
            filename: '[name].css'
        }),
      new CopyWebpackPlugin([
          { from: __dirname + '/node_modules/font-awesome/css/font-awesome.min.css', to: 'font-awesome.min.css' },
          { from: __dirname + '/node_modules/vue/dist/vue.min.js', to: 'vue.min.js' },
          // { from: __dirname + '/node_modules/vue/dist/vue.js', to: 'vue.js' },
          { from: __dirname + '/node_modules/vue-router/dist/vue-router.min.js', to: 'vue-router.min.js' },
          { from: __dirname + '/node_modules/vuex/dist/vuex.min.js', to: 'vuex.min.js' },
          { from: __dirname + '/node_modules/vue-cool-lightbox/dist/vue-cool-lightbox.min.css', to: 'vue-cool-lightbox.min.css' },
      ]),
    ]
};

// entries
config.entry = {
    'medien_fotos': './src/main.js',
};

module.exports = config;
