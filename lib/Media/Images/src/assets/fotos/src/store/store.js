const store = new Vuex.Store({
    state: {
        isLoading: false,
        albums: [],
        lastAlbum: {
            id: -1,
            date: '',
            title: '',
            images: [],
            credits: '',
            flickr: {
                url: '',
            },
        },
        search: {
            year: window.medien_fotos_settings.maxYear,
        },

        // settings
        minYear: 1,
        maxYear: 1,
        flickrApiAvailable: true,
        ...window.medien_fotos_settings,
    },

    mutations: {
        setLoading(state, loading) {
            state.isLoading = loading;
        },

        setAlbums(state, albums) {
            state.albums = albums;
        },
        setLastAlbum(state, album) {
            state.lastAlbum = album;
        },
        setSearch(state, search) {
            state.search = search;
        }
    }
});

export default store;
