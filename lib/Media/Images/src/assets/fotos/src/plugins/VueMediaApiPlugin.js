import store from '../store/store';

const VueMediaPlugin = {
    install(Vue, options) {
        Vue.prototype.$mediaApi = {
            call: (method, data, cb) => {
                store.commit('setLoading', true);
                jQuery.post(window.ajaxUrl, {action: 'medien_fotos', method, ...data}, (res) => {
                    cb(res);
                    store.commit('setLoading', false);
                });
            },
        };
    }
};

export default VueMediaPlugin;
