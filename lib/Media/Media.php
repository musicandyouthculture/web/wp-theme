<?php

namespace ifmayc\Media;

use ifmayc\Media\Images\Fotos;
use PostTypes\Metabox;
use PostTypes\MetaboxField;

class Media
{
    /**
     * Media constructor.
     */
    public function __construct()
    {
        new Fotos();
        new Videos();

        $mediaMeta = new Metabox('Media Menu Meta');
        $mediaMeta->location('nav_menu_item', 'location/media');
        $mediaMeta->field((new MetaboxField('image'))->type('image'));
        $mediaMeta->add();

        $mediaPostMeta = new Metabox('Media Post Meta');
        $mediaPostMeta->posttype('post');
        $mediaPostMeta->field((new MetaboxField('publisher_link'))->type('text'));
        $mediaPostMeta->field((new MetaboxField('publisher_name'))->type('text'));
        $mediaPostMeta->add();
    }
}
