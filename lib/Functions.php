<?php

namespace ifmayc;

use ifmayc\Media\Media;
use PostTypes\Metabox;
use PostTypes\MetaboxField;
use PostTypes\PostType;
use Timber\Timber;

class Functions
{
    /**
     * Functions constructor.
     */
    public function __construct()
    {
        // gigadmin
        new \ifmayc\Gigadmin();

        $bands = new PostType('band', [
            'supports' => ['title', 'editor', 'thumbnail'],
        ]);
        $bands->icon('dashicons-format-audio');
        $bands->register();

        $metabox = new Metabox('Meta');
        $metabox->posttype('band');
        $metabox->field((new MetaboxField('genre'))->type('text'));
        $metabox->field((new MetaboxField('social_link'))->type('text'));
        $metabox->field((new MetaboxField('active', ['default_value' => 1, 'label' => 'Active?']))->type('true_false'));
        $metabox->add();

        // post meta
        $pageMeta = new Metabox('PageMeta');
        $pageMeta->posttype('page');
        $pageMeta->field((new MetaboxField('show_title', ['default_value' => 1, 'label' => 'Show title?']))->type('true_false'));
        $pageMeta->add();

	// disable single
	add_action('template_redirect', function () {
	    if (is_singular('band')) {
		wp_redirect('/bands', 301);
		exit;
	    }
	});

        add_shortcode('ifmayc_ini_bands', [$this, 'shortcodeIfmaycIniBands']);

        // media
        new Media();
    }

    /**
     * add_shortcode callback
     *
     * @param array $atts
     *
     * @return bool|string
     */
    public function shortcodeIfmaycIniBands($atts)
    {
        $atts = shortcode_atts([
            'active' => true,
        ], $atts);

        $bands = Timber::get_posts([
            'post_type' => 'band',
            'meta_key' => 'active',
            'meta_value' => $atts['active'],
        ]);

        return Timber::fetch('shortcodes/ifmayc-ini-bands.twig', compact('bands'));
    }
}
