<?php

namespace ifmayc;

use ifmayc\Gigadmin\Event;
use Timber\Timber;
use ifmayc\Gigadmin\RSS;

class Gigadmin
{
    /**
     * Gigadmin constructor.
     */
    public function __construct()
    {
        if (! defined('GIGADMIN_DB_NAME') || ! defined('GIGADMIN_DB_USER') || ! defined('GIGADMIN_DB_PASSWORD') || ! defined('GIGADMIN_DB_HOST')) {
	    wp_die(
		'<h1>Gigadmin DB Connection variables missing</h1>' .
		'<ul>' .
		'<li>GIGADMIN_DB_NAME</li> <li>GIGADMIN_DB_USER</li> <li>GIGADMIN_DB_PASSWORD</li> <li>GIGADMIN_DB_HOST</li>' .
		'</ul>'
	    );
        }

        global $gigdb;
        $gigdb = new \wpdb(GIGADMIN_DB_USER, GIGADMIN_DB_PASSWORD, GIGADMIN_DB_NAME, GIGADMIN_DB_HOST);

        add_shortcode('gigadmin_events_upcoming', [$this, 'shortcodeGigadminEventsUpcoming']);
        add_shortcode('gigadmin_events_past', [$this, 'shortcodeGigadminEventsPast']);

		new RSS();
    }

    /**
     * add_shortcode callback
     *
     * @return bool|string
     */
    public function shortcodeGigadminEventsUpcoming()
    {
        $events = Event::getUpcoming();

        return Timber::fetch('shortcodes/gigadmin-events-upcoming.twig', compact('events'));
    }

    /**
     * add_shortcode callback
     *
     * @return bool|string
     */
    public function shortcodeGigadminEventsPast()
    {
        $events = Event::getPast();

        return Timber::fetch('shortcodes/gigadmin-events-past.twig', compact('events'));
    }
}

new Gigadmin();
