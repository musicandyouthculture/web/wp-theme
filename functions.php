<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

/**
 * If you are installing Timber as a Composer dependency in your theme, you'll need this block
 * to load your dependencies and initialize Timber. If you are using Timber via the WordPress.org
 * plug-in, you can safely delete this block.
 */
$composer_autoload = __DIR__ . '/vendor/autoload.php';
if ( file_exists( $composer_autoload ) ) {
    require_once $composer_autoload;
    // $timber = new Timber\Timber();
}

/**
 * This ensures that Timber is loaded and available as a PHP class.
 * If not, it gives an error message to help direct developers on where to activate
 */
if ( ! class_exists( 'Timber' ) ) {

	add_action(
		'admin_notices',
		function() {
			echo '<div class="error"><p><b>Timber</b> not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		}
	);

	add_filter(
		'template_include',
		function( $template ) {
		    die("Couldn't find Timber!");
			return get_stylesheet_directory() . '/static/no-timber.html';
		}
	);
	return;
}

if ( ! function_exists( 'acf_add_local_field_group' ) ) {

	add_action(
		'admin_notices',
		function() {
			echo '<div class="error"><p><b>Advanced Custom Fields</b> not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		}
	);

	return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array( 'templates', 'views' );

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;

/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action('wp_enqueue_scripts', [$this, 'enqueueScripts']);

		new \ifmayc\Functions();

		parent::__construct();
	}
	/** This is where you can register custom post types. */
	public function register_post_types() {

	}
	/** This is where you can register custom taxonomies. */
	public function register_taxonomies() {

	}

    public function enqueueScripts()
    {
        wp_enqueue_style('theme', $this->assetsUrl('main.css'));
			wp_enqueue_style('dashicons');
	}

    /** This is where you add some context
     *
     * @param array $context context['this'] Being the Twig's {{ this }}.
     *
     * @return array
     */
	public function add_to_context( array $context ) {
        // add menus
        $context['menu'] = [];
        foreach (get_registered_nav_menus() as $location => $name) {
            $context['menu'][$location] = new TimberMenu($location);
        }

        // wp variables
        $context['WP_DEBUG'] = defined('WP_DEBUG') && WP_DEBUG;

        // sidebars
        $context['sidebar'] = [
            'main' => \Timber\Timber::get_widgets('main'),
        ];
        $context['footer'] = [
            'footer-1' => \Timber\Timber::get_widgets('footer-1'),
            'footer-2' => \Timber\Timber::get_widgets('footer-2'),
            'footer-3' => \Timber\Timber::get_widgets('footer-3'),
        ];

        $context['site']  = $this;

        $context['nextEvent'] = \ifmayc\Gigadmin\Event::getNext();

		return $context;
	}

	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			[
				'gallery',
				'caption',
                'navigation-widgets',
            ]
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );
		register_nav_menus([
		    'main' => 'Main',
            'footer' => 'Footer',
            'media' => 'Medien',
        ]);

        register_sidebar([
            'name'        => 'Sidebar Main',
            'id'          => 'main',
        ]);

        register_sidebar([
            'name'        => 'Footer Links',
            'id'          => 'footer-1',
        ]);
        register_sidebar([
            'name'        => 'Footer Mitte',
            'id'          => 'footer-2',
        ]);
        register_sidebar([
            'name'        => 'Footer Rechts',
            'id'          => 'footer-3',
        ]);
	}

    /** This is where you can add your own functions to twig.
     *
     * @param \Twig\Environment $twig
     *
     * @return string
     */
	public function add_to_twig(Twig\Environment $twig) {
		$twig->addExtension( new Twig\Extension\StringLoaderExtension() );
		$twig->addFunction(new \Twig\TwigFunction('assetsUrl', [$this, 'assetsUrl']));

		return $twig;
	}

	public function assetsUrl($asset)
	{
		return ifmaycThemeAssetsUrl($asset);
	}
}

function ifmaycThemeAssetsUrl($asset)
{
	return get_stylesheet_directory_uri() . "/dist/$asset";
}

new StarterSite();
